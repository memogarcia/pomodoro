#!/usr/bin/env python

import datetime
import subprocess
import time


def send_notification(msg):
    notification = 'notify-send {0} -u critical -t 8000'.format(msg)
    subprocess.check_output(notification, shell=True)


def start_time():
    return datetime.datetime.now()


def time_plus_25_min(start_t):
    return start_t + datetime.timedelta(minutes=25)


def short_break():
    send_notification('"Take a short break"')
    time.sleep(5*60)


def long_break():
    send_notification('"Take a long break"')
    time.sleep(25*60)


def main():
    global break_blocks
    global break_t

    now = start_time()

    if now >= break_t:
        # check if the counter_status is a long_break
        if break_blocks % 4 == 0 and break_blocks > 0:
            long_break()
            break_blocks += 1
            start = start_time()
            break_t = time_plus_25_min(start)
            send_notification('"start working"')
        else:
            short_break()
            break_blocks += 1
            start = start_time()
            break_t = time_plus_25_min(start)
            send_notification('"start working"')

        print(break_blocks)


if __name__ == '__main__':

    break_blocks = 0
    start = start_time()
    break_t = time_plus_25_min(start)
    send_notification('"start working"')

    while True:
        try:
            main()
            time.sleep(10)
        except KeyboardInterrupt:
            break
